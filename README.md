# First HTML Markup

This markup will come in handy at the end of the HTML and CSS course as you will create a full landing page based on this markup. <br/>
Visual presentation on [Figma Project](https://www.figma.com/file/QtgAyrQ9qNLJyHnAQRCcdB/HTML-Essentials-with-styles?type=design&node-id=0-1&mode=design&t=17AHm25tuKQJo4nr-0).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Task

The things you need before installing the software.

* Install any IDE or use the one that is already installed (for example, Visual Studio Code)
* Get your account in a public GitLab repository and create a project named "HTML-Essentials-HomeTask"
* Create a local folder for the future project and start the project in your IDE or folder.

### Installation

A step by step guide that will tell you how to get the development environment up and running.

```
git clone https://gitlab.com/seygorin/html-essentials-hometask.git
```
